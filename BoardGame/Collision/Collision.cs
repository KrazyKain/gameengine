﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Interfaces;

namespace BoardGame.Collision
{
    class Collision
    {
        private iCollidable _entity1;
        private iCollidable _entity2;

        public iCollidable _Entity1 { get { return _entity1; } set { _entity1 = value; } }
        public iCollidable _Entity2 { get { return _entity2; } set { _entity2 = value; } }

        public Collision(iCollidable entity1, iCollidable entity2)
        {
            _Entity1 = entity1;

            _Entity2 = entity2;
        }

    }
}
