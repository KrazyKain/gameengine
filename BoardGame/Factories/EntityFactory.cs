﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using BoardGame.Interfaces;

namespace BoardGame
    {

    class EntityFactory 
    {   

        static private EntityFactory EFactory = null;
    
        private EntityFactory()
        {
        }

        public static EntityFactory getFactory()
        {
            if (EFactory == null)
            {
                EFactory = new EntityFactory();
            }

            return EFactory;
        }

        public T Build<T>() where T : class
        {

            var type = typeof(T);
            return Activator.CreateInstance(type) as T;

        }
    }
}
