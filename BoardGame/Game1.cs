﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


using BoardGame.Managers;
using BoardGame.Interfaces;
using BoardGame.Input;

using BoardGame.Entities;

using BoardGame.Pathfinding;


#endregion

namespace BoardGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
       /* GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;*/


        //   KeyboardState keystate = new KeyboardState();
       
      

        private QuadTreeManager _QuadtreeManager;
        private Renderer _Renderer;

        //Change this to change the game!
        private iGame _TheGame = new WWasteland.WWgame();
       

        public Game1()
            : base()
        {
       
            _Renderer = Renderer.setRenderer(this, Color.AntiqueWhite,1600,900);
            _QuadtreeManager = QuadTreeManager.Manager();
            
            SceneManager._Renderer = _Renderer;


            //CALL INIT FROM GAME
            _TheGame.init(Content);
            
           
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
          //  keystate = Keyboard.GetState();
            IsMouseVisible = true;
            base.Initialize();
        }

      
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.

            _Renderer.setSpritebatch(GraphicsDevice);
            // load game content:

            //CALL LOADCONTENT FROM GAME

            _TheGame.LoadContent();
      
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // Unload content
            Content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            InputHandler.Instance.Update();
            

            if (InputHandler.Instance.keyPressed(new ControlInput(Keys.Space, "NextScene")))
            {

                SceneManager.nextScene();
            }

            if (InputHandler.Instance.keyPressed(new ControlInput(Keys.Escape, "Close")))
            {

                Exit();
            }
         

            if (SceneManager._CurrentScene != null)
            {


                //do something with this!!!!
                CollisionManager.update(SceneManager._CurrentScene);

                AIManager.updateBrains();
            }
            _QuadtreeManager.update();
            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            
         

            _Renderer.drawAll(GraphicsDevice);
          
            base.Draw(gameTime);

            
        }
    }
}

