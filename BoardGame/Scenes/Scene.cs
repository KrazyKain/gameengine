﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using BoardGame.Interfaces;


namespace BoardGame.Scenes
{
        

    class Scene
    {
        private List<iRenderable> _backgroundentities;
        private List<iRenderable> _canvasentities;
        private List<iRenderable> _foregroundentities;

        public List<iRenderable> _BackGroundEntities { get { return _backgroundentities; } }
        public List<iRenderable> _CanvasEntities { get { return _canvasentities; } }
        public List<iRenderable> _ForegroundEntities { get { return _foregroundentities; } }

        private List<iWaypoint> _waypoints;

        public List<iWaypoint> _Waypoints { get { return _waypoints; } }

        public String _SceneName { get { return _sceneName; } set { _sceneName = value; } }
        private String _sceneName;

        public Scene()
        {
            _backgroundentities = new List<iRenderable>();
            _canvasentities = new List<iRenderable>();
            _foregroundentities = new List<iRenderable>();

            _waypoints = new List<iWaypoint>();
        }

        public void initScene(String Name)
        {
            _SceneName = Name;
        }

        public void addWaypoint(iWaypoint wp)
        {
            _waypoints.Add(wp);
        }

        public iWaypoint getNearestWaypoint(Vector2 compareloc)
        {
            iWaypoint nearestwp = null;
            float nearestdistance = 99999;

            foreach (iWaypoint wp in _waypoints)
            {
                Vector2 position = wp.position;
                float distance = (float)Math.Sqrt(((position.X - compareloc.X) * (position.X - compareloc.X)) + ((position.Y - compareloc.Y) * (position.Y - compareloc.Y)));
                if (nearestwp == null)
                {
                    nearestwp = wp;
                    nearestdistance = distance;
                }
                else
                {

                    if (distance < nearestdistance)
                    {
                        nearestwp = wp;
                        nearestdistance = distance;
                    }



                }
            }


            return nearestwp;
        }

        public void addEntity(iRenderable entity, RenderLayer layer, Vector2 location)
        {
            entity._Location = location;
            switch (layer)
            {
                case (RenderLayer.BackGround):
                   _backgroundentities.Add(entity);
                    break;
                case (RenderLayer.Canvas):
                    _canvasentities.Add(entity);
                    break;
                case (RenderLayer.ForeGround):
                    _foregroundentities.Add(entity);
                    break;
            }
        }

        //try to remove the entity from all 3 lists, if not found, exception is caught
        public void removeEntity(iRenderable renderable)
        {
            try
            {
                        _backgroundentities.Remove(renderable);
            } catch {}
            try{
                        _canvasentities.Remove(renderable);
            }catch{}
            try{
                        _foregroundentities.Remove(renderable);
            }
            catch { }
        }
        
       
    }
}
