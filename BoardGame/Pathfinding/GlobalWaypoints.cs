﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;

namespace BoardGame.Pathfinding
{
    class GlobalWaypoints
    {

        private ArrayList waypoints;

        public GlobalWaypoints()
        {
            waypoints = new ArrayList();
        }
 

        public void addWaypoint(Waypoint point)
        {
            waypoints.Add(point);
        }

        public ArrayList getWaypoints()
        {
            return waypoints;
        }


        public Waypoint getWaypoint(int index)
        {
            return (Waypoint)waypoints[index];
        }

    }
}
