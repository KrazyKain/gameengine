﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using BoardGame.Interfaces;

namespace BoardGame.Pathfinding
{
    public class Waypoint : iWaypoint
    {

        //For the seek/pursuit steering
        public Vector2 position { get; set; }
        private List<Link> neighbours;


        public Waypoint(Vector2 pos)
        {
            position = pos;
            neighbours = new List<Link>();
        }


        public void AddNeighbour(iWaypoint point, int w)
        {
            if (neighbours != null)
            {
                Link l = new Link(w, point);
                neighbours.Add(l);
            }
        }


        public List<Link> GetNeighbours()
        {
            return neighbours;
        }


    }

    public class Link
    {
        public int weight;
       public iWaypoint neighbour;

       public Link(int w, iWaypoint point)
       {
            weight = w;
            neighbour = point;
       
       }
    }
}
