﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using BoardGame.Pathfinding;
using BoardGame.Interfaces;

namespace BoardGame.Pathfinding
{
    class Pathfinder
    {
       // public iWaypoint StartWayPoint;

      //  public iWaypoint DestinationWayPoint;

        public iWaypoint activeWaypoint;

        //how many steps do we check before giving up? optimization for bigger maps to avoid checking too far in the wrong direction.
        public int steplimit = 500;
        //list of connections starting from the start way point ending at destination.
        private List<iWaypoint> bestpath;

        private int LowestWeight = 0;

        //all connections in the map.
        public List<Link> connections;


        GlobalWaypoints allWaypoints;


        private int[,] layout = new int[,]
        {
            { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, }, 
            { 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, },
            { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, }, 
            { 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, }, 
            { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
            { 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, }, 
            { 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, }, 
            { 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, }, 
            { 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, }, 
            { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, },
            { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, }, 
            { 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, },
            { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, }, 
        };


        //constructor
        public Pathfinder()
        {
            //Create list of iWaypoints
            allWaypoints = new GlobalWaypoints();
            allWaypoints.addWaypoint(new Waypoint(new Vector2(200, 200)));
            allWaypoints.addWaypoint(new Waypoint(new Vector2(600, 250)));
            allWaypoints.addWaypoint(new Waypoint(new Vector2(600, 600)));
            allWaypoints.addWaypoint(new Waypoint(new Vector2(900, 150)));
            allWaypoints.addWaypoint(new Waypoint(new Vector2(450, 325)));
            allWaypoints.addWaypoint(new Waypoint(new Vector2(100, 600)));

            bestpath = new List<iWaypoint>();

            //Get first iWaypoint in the list - for testing
          //  StartWayPoint = allWaypoints.getWaypoint(0);

            

            //begin the iteration.
          //  step(new List<Waypoint>(), StartWayPoint, steplimit, 0, new List<Waypoint>());


        }


        public List<iWaypoint> findPath(iWaypoint start, iWaypoint end)
        {
            step(new List<iWaypoint>(), start, steplimit, 0, new List<iWaypoint>(),end); // add end to the main method

            return bestpath;
        }

        public List<iWaypoint> findPath(iWaypoint start, iWaypoint end, int steps)
        {
            step(new List<iWaypoint>(), start, steps, 0, new List<iWaypoint>(), end); // add end to the main method

            return bestpath;
        }

        //all the main code here!!
        private void step(List<iWaypoint> laststep, iWaypoint currentwaypoint, int steps, int currentweight, List<iWaypoint> connectionsconsidered, iWaypoint destination)
        {

            int stepcount = steps--;


            //check if we have any steps remaining
            if (stepcount > 0)
            {
                connections = currentwaypoint.GetNeighbours();
                if (currentwaypoint.position == new Vector2(139,452))
                {
                    System.Diagnostics.Debug.WriteLine("I have collided with ");
                }
            //    for (int i = 0; i < connections.Count; i++)
            foreach (Link l in connections)
                {

                    //reset total weight in the loop for when the recursion gets called multiple times in the same run
                    int totalweight = currentweight;
                    //clone the array to ensure we aren't working on the same one in multiple steps
                    List<iWaypoint> path = cloneArray(laststep);
                    List<iWaypoint> previousconnections = cloneArray(connectionsconsidered);

                    //List<Link> links = currentwaypoint.GetNeighbours();

                    //first make sure the current script does not exist in previous connections, so as not to endlessly recheck the same connections
                    if (!previousconnections.Contains(l.neighbour))
                    {
                        //if (conscript.contains(currentwaypoint))
                        {

                            totalweight += l.weight;

                            path.Add(currentwaypoint);
                            if (l.neighbour.position == destination.position)
                            {
                                path.Add(l.neighbour);
                               // Console.WriteLine("destination reached with total weight" + totalweight + "while lowest weight is " + LowestWeight);
                                /*   ListofLists.Add(path);

                                   Debug.Log("Destination reached with" + path.Count + " connections");*/
                                if (LowestWeight == 0 || totalweight < LowestWeight)
                                {
                                    //if there is no best path yet or there is and our total weight is lower than its best, set this path to the best and it's weight to the lowest.
                                    bestpath = path;
                                    LowestWeight = totalweight;

                                }

                            }
                            else
                            {

                                //And previous connections so it doesn't get looked at again.
                                previousconnections.Add(currentwaypoint);

                                //no point in running another recursion if we've already found the destination in a previous run, and have already overtaken the lowest weight.

                                if (LowestWeight == 0 || totalweight < LowestWeight)
                                {
                                    //Console.WriteLine(currentwaypoint.ToString() + "links to " + l.neighbour.ToString());

                                    //now run this function again with the next iWaypoint, an updated stepcount and all the previous connections 
                                    step(path, l.neighbour, stepcount, totalweight, previousconnections,destination);
                                }


                            }
                        }

                    }

                }
            }
        }//End of function


        private List<iWaypoint> cloneArray(List<iWaypoint> toCopy)
        {
            List<iWaypoint> result = new List<iWaypoint>();

            foreach (Waypoint g in toCopy)
            {
                result.Add(g);
            }

            return result;
        }


        //DISJKTRA BRAIN CLASS FUNCTIONS

        public void wpReached(Waypoint wphit)
        {
            if (wphit == activeWaypoint)
            {
                nextWP();
            }
        }



        private void nextWP()
        {
           /* int currentindex = Array.IndexOf(waypointlist, activeWaypoint);

            if (!headingback)
            {
                if (currentindex < iWaypointlist.Length - 1)
                {
                    activeWaypoint = iWaypointlist[++currentindex]; //if not at the end of the array, move up to next iWaypoint
                }
                else
                {
                    headingback = true;
                    nextWP();
                }
            }
            else //if headingback is true
            {
                if (loop)
                {
                    if (currentindex > 0)
                    {
                        activeWaypoint = iWaypointlist[0];
                    }
                    else
                    {
                        headingback = false; //if currentindex is already 0, destination has been reached, and the path can be reset
                        nextWP();
                    }
                }
                else //if not looping, go back on the path
                {
                    if (currentindex > 0)
                    {
                        activeWaypoint = iWaypointlist[--currentindex];
                    }
                    else
                    {
                        headingback = false; //if currentindex is already 0, destination has been reached, and the path can be reset
                        nextWP();
                    }
                }
            }
            lookAt(activeWaypoint);*/
        }


    }
}
