﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Interfaces;
using Microsoft.Xna.Framework;
using BoardGame.Input;
using BoardGame.Managers;

namespace BoardGame.AI
{
    class PlayerControl : EntityBrain
    {


        public float angle=0;
        public int Speed = 4;
        public int xvel = 0;
        public int yvel = 0;

        public PlayerControl(iMoveable token) : base(token)
        {

            InputHandler.addInput(Microsoft.Xna.Framework.Input.Keys.W, "Up");
            InputHandler.addInput(Microsoft.Xna.Framework.Input.Keys.S, "Down");
            InputHandler.addInput(Microsoft.Xna.Framework.Input.Keys.A, "Left");
            InputHandler.addInput(Microsoft.Xna.Framework.Input.Keys.D, "Right");

        }

        private void checkinput()
        {
            float x = _token._Location.X;
            float y = _token._Location.Y;
            xvel = 0;
            yvel = 0;
            if (InputHandler.Instance.checkLabelDown("Up"))
            {
                yvel = -Speed;
                angle =  9.45f;
            }
            if (InputHandler.Instance.checkLabelDown("Down"))
            {
                yvel = Speed;
                angle = 0.0f;
            }
            if (InputHandler.Instance.checkLabelDown("Left"))
            {
                xvel = -Speed;
                angle = 1.51f;
            }
            if (InputHandler.Instance.checkLabelDown("Right"))
            {
                xvel = Speed;
                angle =  4.7f;
            }

            x += xvel;
            y += yvel;
            _token._Location = new Vector2(x, y);

        }

        public override void update()
        {

            checkinput();

        }
    }
}
