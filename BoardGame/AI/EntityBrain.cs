﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Interfaces;
using Microsoft.Xna.Framework;
using BoardGame.Steering;
using Microsoft.Xna.Framework.Input;
using BoardGame.Pathfinding;
using BoardGame.Entities;

namespace BoardGame.AI
{
    class EntityBrain
    {
        protected iMoveable _token;

        protected Vector2 _destination;

        public Vector2 _Destination { get { return _destination; } set { _destination = value; } }

        protected Vector2 _direction;

        protected float xspeed = 0;

        protected float yspeed = 0;

        public Pathfinder _Pathfinder;

        Vector2 entityPrevious = new Vector2();

        public Vector2 direction;

        //Fields needed for pathfinding and to be fed to the motor.
        private iSteering motor;
        Vector2 previousVelocity = new Vector2();
        Vector2 velocity = new Vector2();
        private Boolean returning = false;
        List<Vector2> points;
        int currentPoint = 0;
        int currentPursuePoint = 0;
        int returnPoint = 0;
         List<iWaypoint> waypoints;
         List<iWaypoint> pursueRoute;
         List<iWaypoint> returnRoute;
         STATE currentState;
         STATE previousState;

         Vector2 lastWayPoint;

         KeyboardState previousKSState;

         playerPiece player;

        public EntityBrain(iMoveable token)
        {
            currentState = STATE.PATROLLING;
            //possess thy token!!
            _token = token;
            motor = new SteeringMotor();
            waypoints = new List<iWaypoint>();
            pursueRoute = new List<iWaypoint>();
            returnRoute = new List<iWaypoint>();
            points = new List<Vector2>();
            points.Add(new Vector2(20,20));
            points.Add(new Vector2(1000, 50));
            points.Add(new Vector2(600, 450));
            points.Add(new Vector2(300, 200));
            waypoints = _token.getWaypoints();

            player = (playerPiece)EntityManager.getPlayerToken();

            _Pathfinder = new Pathfinder();
        }

        public void Goto()
        {
            Vector2 difference = new Vector2(_destination.X - _token._Location.X, _destination.Y - _token._Location.Y);

          

            Vector2 diffRat = convertToRatio(difference);

            float directionx = difference.X > 0 ? 1 : -1;
            float directiony = difference.Y > 0 ? 1 : -1;

            _direction = new Vector2(directionx, directiony);

            xspeed = diffRat.X * _token._Speed;
            yspeed = diffRat.Y * _token._Speed;

        }

        public void updatePos()
        {
          
            float x = _token._Location.X;
            float y = _token._Location.Y;

            x += xspeed * _direction.X;

            y += yspeed * _direction.Y;

            _token._Location = new Vector2(x, y);
        }

        Vector2 temporary = new Vector2();

        public virtual void update()
        {
            
            KeyboardState ks = Keyboard.GetState();
            
            velocity = Vector2.Subtract(player._Location, previousVelocity);
            //Change the state of the enemies based on numpad keypress
            if (ks.IsKeyDown(Keys.NumPad1) && previousKSState.IsKeyUp(Keys.NumPad1))
            {
                if (currentState == STATE.PURSUING)
                {
                    currentState = STATE.RETURNING;
                    returnPoint = 0;
                }
                else if (currentState == STATE.PATROLLING)
                {
                    currentState = STATE.PURSUING;
                }
            }

     
            //This state is called after pursuing state has ended. It creates a path back to the patrol path.
            if (currentState == STATE.RETURNING)
            {
                returnRoute = returnPath(currentPoint);
                _token._Location = Vector2.Add(_token._Location, motor.seekTarget(_token._Location, returnToPath()));
            }

            //This state follows the patrol path created by the pathfinder
            if (currentState == STATE.PATROLLING)
            {
                _token._Location = Vector2.Add(_token._Location, motor.seekTarget(_token._Location, pathFollow()));  
            }

            //Follows the player via a path made from waypoints.
            if (currentState == STATE.PURSUING)
            {
                if (pursueRoute.Count <= 2)
                {
                    pursueRoute = updatePath();
                }

                //The current waypoint is fed to the motor through the pursueFollow method.
                _token._Location = Vector2.Add(_token._Location, motor.seekTarget(_token._Location, pursueFollow()));
            }

            //Chases the player when they are below a distance threshold.
            if (currentState == STATE.CHASE)
            {
                //Feed player location to the motor
                _token._Location = Vector2.Add(_token._Location, motor.seekTarget(_token._Location, player._Location));
                //If the entity is above the player distance threshold a new pursue list is made and the state is changed to pursuing.
                if (Vector2.Distance(_token._Location, player._Location) > 150)
                {
                    pursueRoute.Clear();
                    pursueRoute = updatePath();
                    currentPursuePoint = 0;
                    currentState = STATE.PURSUING;
                }
            }

            previousState = currentState;

            //Convert entity direction to normalised direction to use for raycasting
            direction = convertToRatio(motor.getVelocity());
            previousVelocity = player._Location;
            entityPrevious = _token._Location;
            previousKSState = ks;
        }

        public void addWaypointToPath(iWaypoint point)
        {
            waypoints.Add(point);
        }

        //Creates vector denoting direction
        private Vector2 convertToRatio(Vector2 v)
        {
            double posxdif = v.X > 0 ? v.X : (v.X * -1);
            double posydif = v.Y > 0 ? v.Y : (v.Y * -1);

            double totaldif = posxdif + posydif;

            float xRat = (float)(posxdif / totaldif);
            float yRat = (float)(posydif / totaldif);

            xRat = v.X > 0 ? xRat : -xRat;
            yRat = v.Y > 0 ? yRat : -yRat;


            return new Vector2(xRat, yRat);
        }


        //Iterates through the waypoints to get the current/next destination 
        private Vector2 pathFollow()
        {
            Vector2 target = new Vector2();
            target = _token._waypoints[currentPoint].position;

            //If the entity is a certain distance from the target the next waypoint can be chosen, helps with steering
                if (Vector2.Distance(_token._Location, target) <= 10)
                {
                    if (!returning)
                    {
                        currentPoint++;
                    } 
                    else
                    {
                        currentPoint--;
                    }
                    
                    if (currentPoint >= _token._waypoints.Count)
                    {
                        returning = true;
                        currentPoint--;
                    }    
                    else if (currentPoint <= 0)
                    {
                        returning = false;
                    }
                }
            
            return target;
        }

        //Return the next waypoint in the path to the patrol point
        private Vector2 returnToPath()
        {
            Vector2 target = new Vector2();

            //If the entity has reached the end of the path they are at the patrol waypoint, ergo, change state.
            if (returnPoint >= returnRoute.Count)
            {
                currentState = STATE.PATROLLING;
                return target;
            }
            else
            {
                target = returnRoute[returnPoint].position;

                //If the entity is a certain distance from the target the next waypoint can be chosen, helps with steering
                if (Vector2.Distance(_token._Location, target) <= 10)
                {
                    returnPoint++;
                }
                return target;
            }      
        }

        //Iterates through the waypoints to get the current/next destination 
        private Vector2 pursueFollow()
        {
            Vector2 target = new Vector2();

            //If the entity is close enough the player the state is changed to a direct chase.
            if (Vector2.Distance(_token._Location, player._Location) < 150)
            {
                currentState = STATE.CHASE;
                return new Vector2();
            }
            else
            {
                if (currentPursuePoint >= pursueRoute.Count)
                {
                    pursueRoute = updatePath();
                    currentPursuePoint = 0;
                }
                //Try catch used to avoid out of bound errors when the entity is close to the player and the pursue route has no waypoints.
                try
                {
                    target = pursueRoute[currentPursuePoint].position;
                }
                catch
                {
                    return player._Location;
                }

                //If the entity is a certain distance from the target the next waypoint can be chosen, helps with steering
                if (Vector2.Distance(_token._Location, target) <= 10)
                {
                    currentPursuePoint++;
                }
            }
            lastWayPoint = target;
            return target;
        }

      
        //Create a list of waypoints between the entity and the player
        private List<iWaypoint> updatePath()
        {
            
            iWaypoint p1startwaypoint = SceneManager.getScene("First Scene").getNearestWaypoint(_token._Location);
            iWaypoint p1destinationwaypoint = SceneManager.getScene("First Scene").getNearestWaypoint(player._Location);
            Pathfinder _tempPathfinder = new Pathfinder() ;
            List<iWaypoint> route = _tempPathfinder.findPath(p1startwaypoint, p1destinationwaypoint, 50);
            
            return route;
        }

        //Create a list of waypoints back the patrol route. The destination is the last point used in the patrol.
        private List<iWaypoint> returnPath(int index)
        {

            iWaypoint p1startwaypoint = SceneManager.getScene("First Scene").getNearestWaypoint(_token._Location);
            iWaypoint p1destinationwaypoint = _token._waypoints[index];
            Pathfinder _tempPathfinder = new Pathfinder();
            List<iWaypoint> route = _tempPathfinder.findPath(p1startwaypoint, p1destinationwaypoint, 50);
            return route;
        }
    }


    public enum STATE
    {
        PATROLLING,PURSUING,RETURNING,CHASE
    }
}
