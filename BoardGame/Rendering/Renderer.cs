﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections;
using BoardGame.Interfaces;

namespace BoardGame
{
    class Renderer
    {
        static private Renderer _theRenderer;
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Game _game;
        //This list will contain all the objects in the game to be rendered, simply adding something to this list will ensure it gets rendered.
        //removing it will ensure it won't.
        private List<iRenderable> RenderListBackground;
        private List<iRenderable> RenderListCanvas;
        private List<iRenderable> RenderListForeground;
        private Color _clearColour;

       static public Renderer setRenderer(Game game, Color clearcolour, int resw, int resh)
        {

            _theRenderer = new Renderer(game,clearcolour,resw,resh);
            return _theRenderer;

        }

        static public Renderer _Renderer()
        {
            return _theRenderer;
        }

        public Vector2 getResolution()
        {
            Vector2 resolution = new Vector2(_graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight);

            return resolution;
        }

        private Renderer(Game game, Color clearcolour, int resw, int resh)
        {
            _clearColour = clearcolour;
            _game = game;
            _graphics = new GraphicsDeviceManager(_game);
            _graphics.IsFullScreen = false;
            _graphics.PreferredBackBufferHeight = resh;
            _graphics.PreferredBackBufferWidth = resw;
            RenderListBackground = new List<iRenderable>();
            RenderListCanvas = new List<iRenderable>();
            RenderListForeground = new List<iRenderable>();
            
        }
        

        public void setSpritebatch(GraphicsDevice device)
        {
            _spriteBatch = new SpriteBatch(device);
        }


        public void addToRenderList(iRenderable renderable, RenderLayer layer)
        {
            switch(layer)
            {
                case(RenderLayer.BackGround):
                    RenderListBackground.Add(renderable);
                    break;
                case(RenderLayer.Canvas):
                    RenderListCanvas.Add(renderable);
                    break;
                case(RenderLayer.ForeGround):
                    RenderListForeground.Add(renderable);
                    break;
            }
        }

        public void removeFromRenderList(iRenderable renderable, RenderLayer layer)
        {
            try
            {
                 switch(layer)
                    {
                    case(RenderLayer.BackGround):
                        RenderListBackground.Remove(renderable);
                         break;
                     case(RenderLayer.Canvas):
                         RenderListCanvas.Remove(renderable);
                         break;
                     case(RenderLayer.ForeGround):
                         RenderListForeground.Remove(renderable);
                         break;
                 }
            }
            catch { }
        }

        //method to draw everything, requires the game.graphicsdevice
        public void drawAll(GraphicsDevice device)
        {
            //clear the screen
            device.Clear(_clearColour);
            
            _spriteBatch.Begin();

            //draw everything in the RenderLists, doing them in order so the background gets drawn first, then the canvas appears over it, and the foreground on top
            foreach (iRenderable renderable in RenderListBackground)
            {
                renderable.draw(_spriteBatch);
            }
            foreach (iRenderable renderable in RenderListCanvas)
            {
                renderable.draw(_spriteBatch);
            }
            foreach (iRenderable renderable in RenderListForeground)
            {
                renderable.draw(_spriteBatch);
            }

            _spriteBatch.End();
        }

        public void clearLayer(RenderLayer layer)
        {
            switch (layer)
            {
                case (RenderLayer.BackGround):
                RenderListBackground = new List<iRenderable>();
                break;
                case (RenderLayer.Canvas):
                RenderListCanvas = new List<iRenderable>();
                break;
                case (RenderLayer.ForeGround):
                RenderListForeground = new List<iRenderable>();
                break;
            }
        }

        public void clearAll()
        {
            clearLayer(RenderLayer.BackGround);
            clearLayer(RenderLayer.Canvas);
            clearLayer(RenderLayer.ForeGround);
        }
        
    }
}
