﻿

using Microsoft.Xna.Framework.Content;


namespace BoardGame.Interfaces
{
    interface iGame
    {

        void init(ContentManager content);

        void LoadContent();


    }
}
