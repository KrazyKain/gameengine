﻿using System;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoardGame.Interfaces
{
    interface iCollidable
    {

        Texture2D _Image { get; set; }


        Vector2 _Location { get; set; }
    
        void OnCollision(iCollidable other);

    }
}
