﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using BoardGame.Pathfinding;

namespace BoardGame.Interfaces
{
    public interface iWaypoint
    {
        Vector2 position { get; set; }
        void AddNeighbour(iWaypoint point, int w);
        List<Link> GetNeighbours();


    }
}
