﻿using System;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoardGame.Interfaces
{
    interface iRenderable
    {
        Texture2D _Image { get; set; }

       
        Vector2 _Location { get; set; }

        //renderables will tell the renderer how they need to be drawn, this will allow cards to display text by overriding this method.
        void draw(SpriteBatch spritebatch);
        String toString();
        
    }
}
