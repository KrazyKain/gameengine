﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using BoardGame.AI;
using Microsoft.Xna.Framework.Graphics;
using BoardGame.Pathfinding;

namespace BoardGame.Interfaces
{
    interface iMoveable
    {
        Vector2 _Location { get; set; }
        float _Speed { get; set; }

        EntityBrain _Brain { get; set; }

        Texture2D _Image { get; set; }

        List<iWaypoint> _waypoints { get; set; }

        void addWaypointToPath(iWaypoint point);

        List<iWaypoint> getWaypoints();
    }
}
