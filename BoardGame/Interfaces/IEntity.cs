﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BoardGame
{
    interface IEntity
    {
        void update();

        //the public accessors will only need a get method as once the private accessor
        //is set, it should never change.
        int _UID { get;  }
        String _UName { get;  }

        Vector2 _Location { get; set; }

        void initEntity(String UniqueName, int UID);
        

      

    }
}
