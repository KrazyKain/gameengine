﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BoardGame.Interfaces
{
    interface iSteering
    {
        Vector2 seekTarget(Vector2 position, Vector2 target);

        Vector2 pursueTarget(Vector2 targetPosition, Vector2 targetVelocity);

        Vector2 getVelocity();
    }
}
