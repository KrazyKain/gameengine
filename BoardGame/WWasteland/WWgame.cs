﻿
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using BoardGame.Interfaces;


using BoardGame.Entities;

using BoardGame.Pathfinding;

namespace BoardGame.WWasteland
{
    class WWgame : iGame
    {

        Texture2D _Map;

        Texture2D _Button;
        Texture2D _Door;
        Texture2D _Monster;
        Texture2D _Collect;
        Texture2D _Player;
        
        Texture2D _VertWall;
        Texture2D _HoriWall;
        Renderer _Renderer;


        const String MenuScene = "Main Menu";

        const String Level1 = "First Scene";

        ContentManager Content;

        public void init(ContentManager content)
        {
            _Renderer = Renderer._Renderer();
            Content = content;
            SceneManager.createScene(MenuScene);

            SceneManager.nextScene();


            SceneManager.createScene(Level1);
            addWaypoints();
            Content.RootDirectory = "Content";
        }


        public void LoadContent()
        {
            
           
            // load game content:

            _Map = Content.Load<Texture2D>("newMap");
            _Button = Content.Load<Texture2D>("StartScreen");
            _Monster = Content.Load<Texture2D>("Monster");
            _Collect = Content.Load<Texture2D>("Pill");
            _Player = Content.Load<Texture2D>("Player");
            _Door = Content.Load<Texture2D>("Door");
            


            MenuItem nextButton = (MenuItem)EntityManager.createEntity<MenuItem>("menuitem", _Button);
            SceneManager.addToScene(MenuScene, nextButton, RenderLayer.Canvas, new Vector2(0, 0));

            Board bg = (Board)EntityManager.createEntity<Board>("Board", _Map);

            Vector2 res = _Renderer.getResolution();
            SceneManager.addToScene(Level1, bg, RenderLayer.BackGround, new Vector2(res.X / 2, res.Y / 2));

            playerPiece p3 = (playerPiece)EntityManager.createEntity<playerPiece>("TestPiece3", _Player);
            EntityManager.setPlayerToken(p3);
            Vector2 playervector = new Vector2(100, 400);
            p3.setspawn(playervector);
            SceneManager.addToScene(Level1, p3, RenderLayer.Canvas, playervector);

            Monster p1 = (Monster)EntityManager.createEntity<Monster>("TestPiece1", _Monster);

            SceneManager.addToScene(Level1, p1, RenderLayer.Canvas, new Vector2(150, 650));



            addWalls();

            iWaypoint p1startwaypoint = SceneManager.getScene(Level1).getNearestWaypoint(p1._Location);
            iWaypoint p1destinationwaypoint = SceneManager.getScene(Level1).getNearestWaypoint(new Vector2(1450, 750));
            p1._waypoints = p1.getPathfinder().findPath(p1startwaypoint, p1destinationwaypoint);




            Collectable p4 = (Collectable)EntityManager.createEntity<Collectable>("TestPiece4", _Collect);


            SceneManager.addToScene(Level1, p4, RenderLayer.Canvas, new Vector2(715, 600));

            Collectable p5 = (Collectable)EntityManager.createEntity<Collectable>("TestPiece5", _Collect);


            SceneManager.addToScene(Level1, p5, RenderLayer.Canvas, new Vector2(134, 120));

            Monster p2 = (Monster)EntityManager.createEntity<Monster>("TestPiece2", _Monster);
            SceneManager.addToScene(Level1, p2, RenderLayer.Canvas, new Vector2(456, 650));


            Door door = (Door)EntityManager.createEntity<Door>("door", _Door);
            door.setExit(MenuScene);
            SceneManager.addToScene(Level1, door, RenderLayer.Canvas, new Vector2(40, 350));




            iWaypoint p2startwaypoint = SceneManager.getScene(Level1).getNearestWaypoint(p2._Location);
            iWaypoint p2destinationwaypoint = SceneManager.getScene(Level1).getNearestWaypoint(new Vector2(1300, 350));

            p2._waypoints = p2.getPathfinder().findPath(p2startwaypoint, p2destinationwaypoint);
            p2._Speed = 1;
            // p2._Brain._Destination = new Vector2(900, 500);
        }


        public void addWaypoints()
        {

            //  Create waypoints
            Waypoint w1 = new Waypoint(new Vector2(160, 100));
            Waypoint w2 = new Waypoint(new Vector2(160, 350));
            Waypoint w3 = new Waypoint(new Vector2(160, 545));
            Waypoint w4 = new Waypoint(new Vector2(160, 780));
            Waypoint w5 = new Waypoint(new Vector2(435, 100));
            Waypoint w6 = new Waypoint(new Vector2(435, 350));
            Waypoint w7 = new Waypoint(new Vector2(435, 545));
            Waypoint w8 = new Waypoint(new Vector2(435, 760));
            Waypoint w9 = new Waypoint(new Vector2(690, 100));
            Waypoint w10 = new Waypoint(new Vector2(690, 350));
            Waypoint w11 = new Waypoint(new Vector2(690, 535));
            Waypoint w12 = new Waypoint(new Vector2(575, 760));
            Waypoint w13 = new Waypoint(new Vector2(880, 100));
            Waypoint w14 = new Waypoint(new Vector2(880, 350));
            Waypoint w15 = new Waypoint(new Vector2(880, 535));
            Waypoint w16 = new Waypoint(new Vector2(760, 760));
            Waypoint w17 = new Waypoint(new Vector2(1080, 100));
            Waypoint w18 = new Waypoint(new Vector2(1080, 350));
            Waypoint w19 = new Waypoint(new Vector2(1080, 535));
            Waypoint w20 = new Waypoint(new Vector2(1080, 760));
            Waypoint w21 = new Waypoint(new Vector2(1310, 350));
            Waypoint w22 = new Waypoint(new Vector2(1310, 545));
            Waypoint w23 = new Waypoint(new Vector2(1480, 100));
            Waypoint w24 = new Waypoint(new Vector2(1480, 350));
            Waypoint w25 = new Waypoint(new Vector2(1480, 545));
            Waypoint w26 = new Waypoint(new Vector2(1480, 760));

            //create connections
            w1.AddNeighbour(w2, 2);

            w2.AddNeighbour(w1, 2);
            w2.AddNeighbour(w6, 2);

            w3.AddNeighbour(w4, 2);
            w3.AddNeighbour(w7, 2);

            w4.AddNeighbour(w3, 2);

            w5.AddNeighbour(w6, 2);
            w5.AddNeighbour(w9, 5);

            w6.AddNeighbour(w2, 2);
            w6.AddNeighbour(w5, 2);
            w6.AddNeighbour(w7, 2);

            w7.AddNeighbour(w3, 2);
            w7.AddNeighbour(w6, 2);
            w7.AddNeighbour(w8, 2);
            w7.AddNeighbour(w11, 2);

            w8.AddNeighbour(w7, 2);
            w8.AddNeighbour(w12, 1);

            w9.AddNeighbour(w5, 5);
            w9.AddNeighbour(w10, 2);
            w9.AddNeighbour(w13, 2);

            w10.AddNeighbour(w9, 2);
            w10.AddNeighbour(w11, 2);
            w10.AddNeighbour(w14, 5);

            w11.AddNeighbour(w7, 2);
            w11.AddNeighbour(w10, 2);
            w11.AddNeighbour(w15, 2);

            w12.AddNeighbour(w8, 1);

            w13.AddNeighbour(w9, 2);

            w14.AddNeighbour(w10, 5);
            w14.AddNeighbour(w15, 5);
            w14.AddNeighbour(w18, 5);

            w15.AddNeighbour(w11, 2);
            w15.AddNeighbour(w14, 5);
            w15.AddNeighbour(w19, 2);

            w16.AddNeighbour(w20, 5);

            w17.AddNeighbour(w18, 2);
            w17.AddNeighbour(w23, 3);

            w18.AddNeighbour(w14, 5);
            w18.AddNeighbour(w17, 2);
            w18.AddNeighbour(w19, 2);

            w19.AddNeighbour(w15, 2);
            w19.AddNeighbour(w18, 2);
            w19.AddNeighbour(w20, 2);

            w20.AddNeighbour(w16, 5);
            w20.AddNeighbour(w19, 2);
            w20.AddNeighbour(w26, 3);

            w21.AddNeighbour(w22, 1);
            w21.AddNeighbour(w24, 1);

            w22.AddNeighbour(w21, 1);
            w22.AddNeighbour(w25, 1);

            w23.AddNeighbour(w17, 3);
            w23.AddNeighbour(w24, 2);

            w24.AddNeighbour(w21, 1);
            w24.AddNeighbour(w23, 2);

            w25.AddNeighbour(w22, 1);
            w25.AddNeighbour(w26, 2);

            w26.AddNeighbour(w20, 2);
            w26.AddNeighbour(w25, 3);

            //add waypoints to scene
            SceneManager.addWaypointToScene(Level1, w1);
            SceneManager.addWaypointToScene(Level1, w2);
            SceneManager.addWaypointToScene(Level1, w3);
            SceneManager.addWaypointToScene(Level1, w4);
            SceneManager.addWaypointToScene(Level1, w5);
            SceneManager.addWaypointToScene(Level1, w6);
            SceneManager.addWaypointToScene(Level1, w7);
            SceneManager.addWaypointToScene(Level1, w8);
            SceneManager.addWaypointToScene(Level1, w9);
            SceneManager.addWaypointToScene(Level1, w10);
            SceneManager.addWaypointToScene(Level1, w11);
            SceneManager.addWaypointToScene(Level1, w12);
            SceneManager.addWaypointToScene(Level1, w13);
            SceneManager.addWaypointToScene(Level1, w14);
            SceneManager.addWaypointToScene(Level1, w15);
            SceneManager.addWaypointToScene(Level1, w16);
            SceneManager.addWaypointToScene(Level1, w17);
            SceneManager.addWaypointToScene(Level1, w18);
            SceneManager.addWaypointToScene(Level1, w19);
            SceneManager.addWaypointToScene(Level1, w20);
            SceneManager.addWaypointToScene(Level1, w21);
            SceneManager.addWaypointToScene(Level1, w22);
            SceneManager.addWaypointToScene(Level1, w23);
            SceneManager.addWaypointToScene(Level1, w24);
            SceneManager.addWaypointToScene(Level1, w25);
            SceneManager.addWaypointToScene(Level1, w26);

        }


        public void addWalls()
        {
            _VertWall = Content.Load<Texture2D>("VerticalWall");
            _HoriWall = Content.Load<Texture2D>("HorizontalWall");

            wall vWall1 = (wall)EntityManager.createEntity<wall>("vWall1", _VertWall);
            wall vWall2 = (wall)EntityManager.createEntity<wall>("vWall2", _VertWall);
            wall vWall3 = (wall)EntityManager.createEntity<wall>("vWall3", _VertWall);
            wall vWall4 = (wall)EntityManager.createEntity<wall>("vWall4", _VertWall);
            wall vWall5 = (wall)EntityManager.createEntity<wall>("vWall5", _VertWall);
            wall vWall6 = (wall)EntityManager.createEntity<wall>("vWall6", _VertWall);
            wall vWall7 = (wall)EntityManager.createEntity<wall>("vWall7", _VertWall);

            wall hWall1 = (wall)EntityManager.createEntity<wall>("hWall1", _HoriWall);
            wall hWall2 = (wall)EntityManager.createEntity<wall>("hWall2", _HoriWall);
            wall hWall3 = (wall)EntityManager.createEntity<wall>("hWall3", _HoriWall);
            wall hWall4 = (wall)EntityManager.createEntity<wall>("hWall4", _HoriWall);
            wall hWall5 = (wall)EntityManager.createEntity<wall>("hWall5", _HoriWall);
            wall hWall6 = (wall)EntityManager.createEntity<wall>("hWall6", _HoriWall);
            wall hWall7 = (wall)EntityManager.createEntity<wall>("hWall7", _HoriWall);
            wall hWall8 = (wall)EntityManager.createEntity<wall>("hWall8", _HoriWall);

            SceneManager.addToScene(Level1, vWall1, RenderLayer.Canvas, new Vector2(300, 120));
            SceneManager.addToScene(Level1, vWall2, RenderLayer.Canvas, new Vector2(300, 765));
            SceneManager.addToScene(Level1, vWall3, RenderLayer.Canvas, new Vector2(568, 325));
            SceneManager.addToScene(Level1, vWall4, RenderLayer.Canvas, new Vector2(666, 765));
            SceneManager.addToScene(Level1, vWall5, RenderLayer.Canvas, new Vector2(968, 120));
            SceneManager.addToScene(Level1, vWall6, RenderLayer.Canvas, new Vector2(1194, 332));
            SceneManager.addToScene(Level1, vWall7, RenderLayer.Canvas, new Vector2(1194, 525));

            SceneManager.addToScene(Level1, hWall1, RenderLayer.Canvas, new Vector2(145, 457));
            SceneManager.addToScene(Level1, hWall2, RenderLayer.Canvas, new Vector2(645, 650));
            SceneManager.addToScene(Level1, hWall3, RenderLayer.Canvas, new Vector2(795, 650));
            SceneManager.addToScene(Level1, hWall4, RenderLayer.Canvas, new Vector2(895, 650));
            SceneManager.addToScene(Level1, hWall5, RenderLayer.Canvas, new Vector2(887, 220));
            SceneManager.addToScene(Level1, hWall6, RenderLayer.Canvas, new Vector2(1280, 250));
            SceneManager.addToScene(Level1, hWall7, RenderLayer.Canvas, new Vector2(1277, 624));
            SceneManager.addToScene(Level1, hWall8, RenderLayer.Canvas, new Vector2(1500, 450));

        }



    }
}
