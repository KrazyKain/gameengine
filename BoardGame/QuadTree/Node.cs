﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using BoardGame.Interfaces;
using BoardGame.Managers;


namespace BoardGame.QuadTree
{
    class Node
    {

        private List<nodeEntry> _Collidables;
        private Vector2 _Size;
        private Vector2 _Location;
        private Node[] _SubNodes;
        private int _Presplitlimit = 1;
        private Node _Parent;
        private bool recheckRequired;
        private int[] _ObjectsPerNode;



        public Node(List<iCollidable> pCollidables, Vector2 pSize, Vector2 pLocation)
        {
            recheckRequired = false;

            _Size = pSize;
            _Location = pLocation;
            _SubNodes = new Node[4];


            _Collidables = new List<nodeEntry>();

            //clone the collidable parameter array
            foreach (iCollidable obj in pCollidables)
            {
                nodeEntry ne = new nodeEntry(obj, identifyNodes(obj));
                _Collidables.Add(ne);

            }



        }



        public List<String> getUIDs()
        {
            List<String> UIDs = new List<String>();

            foreach (nodeEntry NE in _Collidables)
            {


                IEntity entity = (IEntity)NE._Obj;

                UIDs.Add(entity._UName);
            }

            return UIDs;
        }

        private bool CompareSubNodeObjects(int[] oldobjects, int[] newobjects)
        {
            bool match = true;


            for (int i = 0; i < oldobjects.Length; i++)
            {
                if (oldobjects[i] != newobjects[i])
                {
                    match = false;
                }
            }

            return match;
        }

        public void recalculate()
        {
            refreshNode();
            //List<String>[] Latestsubnodeobjects = fillSubnodeObjects(); //fill this in before updating the array to compare.
            // int[] currentframeObjects = nodeCount();
            //  if (!CompareSubNodeObjects(_ObjectsPerNode, currentframeObjects))   //if the latest nodes do NOT match the current nodes, than refresh.
            {

                for (int i = 0; i < _SubNodes.Length; i++) //clear the subnodes
                {
                    _SubNodes[i] = null;
                }
                checkSplit();
            }

            //  _ObjectsPerNode = nodeCount(); //update the nodecount
        }

        public void AddParent(Node pParent)
        {
            _Parent = pParent;
        }

        public void update()
        {
          
                refreshNode();
            //  recheckRequired = false;
            try
            {
                if (_SubNodes[0] == null)
                {
                    //if you have no subnodes refresh yourself
                    checkSplit();
                }
                else
                {

                    for (int i = 0; i < _SubNodes.Length; i++)
                    {

                        _SubNodes[i].update(); //if you have subnodes, tell them to update themselves!
                    }

                }
            }
            catch
            {
                collisioncheck();
            }
                if (recheckRequired && _Parent != null)
                {
                    _Parent.recalculate();
                }
            

        }

        private int[] nodeCount()
        {
            int[] objectpernode = { 0, 0, 0, 0 };

            foreach (nodeEntry ne in _Collidables)
            {
                for (int i = 0; i < ne._NodeExists.Length; i++)
                {
                    if (ne._NodeExists[i])
                    {
                        objectpernode[i]++;  //the index to nodeexists matches the node in objectpernode, therefore that entry gets iterated
                    }
                }
            }

            return objectpernode;
        }

        private void checkSplit()
        {
            if (_Collidables.Count() > 1) //no point in checking for a split or collision if there is only one object (or none!)
            {
                int[] objectpernode = nodeCount();

                int fullnodes = 0;
                bool presplitlimitreached = false;
                for (int i = 0; i< objectpernode.Length;i++)
                {
                    fullnodes += objectpernode[i] > 0 ? 1 : 0; //increment fullnodes if the node being checked has anything in it

                    if (objectpernode[i] > _Presplitlimit) //make sure there are enough objects in the node to be worth splitting
                    {
                        presplitlimitreached = true;
                    }
                }

                if (fullnodes > 1 && presplitlimitreached && _Parent == null)
                {
                    split();
                }
                else
                {
                    collisioncheck();
                }

            }
        }

        private void collisioncheck()
        {
            List<iCollidable> testlist = new List<iCollidable>();

            foreach (nodeEntry ne in _Collidables)
            {
                testlist.Add(ne._Obj);
            }
            CollisionManager.collisionCheck(testlist);

        }

        public void refreshNode()
        {
            List<nodeEntry> tempcollidables = new List<nodeEntry>();

            foreach (nodeEntry ne in _Collidables)
            {
                // ne._CornerNodes = identifyNodes(ne._Obj);
                // ne.setActiveNodes();

                iCollidable tempObj = ne._Obj;
                tempcollidables.Add(new nodeEntry(tempObj,identifyNodes(tempObj)));

            }
            _Collidables = tempcollidables;
        }

        public void split()
        {
            refreshNode();
            //all nodes will be a quarter of the size by halving both height and length
            Vector2 nodesize = new Vector2((float)(_Size.X * 0.5), (float)(_Size.Y * 0.5));

            Vector2[] nodepositions = new Vector2[4];
            nodepositions[0] = new Vector2(_Location.X, _Location.Y);   //upper left node
            nodepositions[1] = new Vector2(_Location.X + nodesize.X, _Location.Y); //upper right node
            nodepositions[2] = new Vector2(_Location.X, _Location.Y + nodesize.Y); //lower left node
            nodepositions[3] = new Vector2(_Location.X + nodesize.X, _Location.Y + nodesize.Y); //lower right node

            List<iCollidable> node0col = new List<iCollidable>();
            List<iCollidable> node1col = new List<iCollidable>();
            List<iCollidable> node2col = new List<iCollidable>();
            List<iCollidable> node3col = new List<iCollidable>();

            List<iCollidable>[] nodecollists = { node0col, node1col, node2col, node3col };


            foreach (nodeEntry ne in _Collidables)
            {
                for (int i = 0; i<ne._NodeExists.Length;i++)
                {
                    if (ne._NodeExists[i]) //if true, the object exists in node i (0 to 3)
                    {
                        nodecollists[i].Add(ne._Obj); //each list in nodecollists has the same index as the corresponding nodeexists boolean so put it in that list.
                    }
                }
            }

            for (int i = 0; i <_SubNodes.Length; i++)
            {
                Node theNode = new Node(nodecollists[i], nodesize, nodepositions[i]);
                theNode.AddParent(this);
                _SubNodes[i] = theNode; 
            }
            recheckRequired = false;
            _ObjectsPerNode = nodeCount();
        }

       private int[] identifyNodes(iCollidable obj)
        {
            try
            {
                //define the corners of the object
                Vector2 topLeftCorner = new Vector2(obj._Location.X, obj._Location.Y);
                Vector2 topRightCorner = new Vector2(obj._Location.X + obj._Image.Width, obj._Location.Y);
                Vector2 botLeftCorner = new Vector2(obj._Location.X, obj._Location.Y + obj._Image.Height);
                Vector2 botRightCorner = new Vector2(obj._Location.X + obj._Image.Width, obj._Location.Y + obj._Image.Height);

                int[] corners = { checkPoint(topLeftCorner), checkPoint(topRightCorner), checkPoint(botLeftCorner), checkPoint(botRightCorner) }; //top left, top right, bottom left, bottom right;
                return corners;
            }
            catch
            {
                return null;
            }

           

        }

        private int checkPoint(Vector2 pPoint)
        {
            int result = -1;
            //first make sure the point is in the node at all, otherwise the results will be broken
            if ( (pPoint.X > _Location.X) && (pPoint.X < _Location.X + _Size.X) && (pPoint.Y > _Location.Y) && (pPoint.Y < _Location.Y + _Size.Y ))
            {
                float middleX = (float)(_Size.X * 0.5);
                float middleY = (float)(_Size.Y * 0.5);
                //we already confirmed that the point is in the node so we only need to check if the point is before or after the halfway point to figure out what subnode it goes in.

                result += pPoint.X < middleX ? 1: 2; //if on the left of the middle, add 1, turning -1 to 0 (top left) or add 2 turning it into 1 (top right)
                result += pPoint.Y < middleY ? 0 : 2; //if over the middle point, don't add anything, staying on the top, if under the middlepoint add 2 turning top right into bottom right or top left to bottom left.



            }
            else
            {
                recheckRequired = true; //parent node needs to recheck as the object is outside this node!
            }
            //if not in the node, return -1

            return result;
        }

        class nodeEntry //wooo nested class
        {
            private int[] _cornernodes;
            private iCollidable _obj;
            public int[] _CornerNodes { get { return _cornernodes; }  set { _cornernodes = value; } }
            public iCollidable _Obj { get { return _obj; } }

            private bool[] _nodeExists = { false, false, false, false };
         

            public bool[] _NodeExists { get { return _nodeExists; } }

            public nodeEntry(iCollidable pobj, int[] pCorners)
            {
                _cornernodes = pCorners;
                _obj = pobj;

                setActiveNodes();

            }

            public void setActiveNodes()
            {
                try
                {
                    for (int i = 0; i < _nodeExists.Length; i++)
                    {
                        _nodeExists[i] = false;
                    }

                    for (int i = 0; i < _cornernodes.Length; i++)
                    {
                        int index = _cornernodes[i];
                        if (index > -1)
                        {
                            if (!_nodeExists[index])
                            {
                                _nodeExists[index] = true;
                                // System.Diagnostics.Debug.WriteLine(_obj.ToString() + " exists in node " + index);
                            }
                        }

                    }
                }
                catch
                { }
            }

        }

        
    }
}
