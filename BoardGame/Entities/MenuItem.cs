﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using BoardGame.Scenes;
using BoardGame.Interfaces;

namespace BoardGame
{
    class MenuItem : BGEntity, iRenderable
    {

        private Texture2D _image;
        public Texture2D _Image { get { return _image;} set { _image = value;}  }

        private Vector2 _location;
        public Vector2 _Location { get { return _location; } set { _location = value; } }

        private Scene _nextScene;

        public MenuItem()
        {
 
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_Image, _Location);
        }

        public void changeScene()
        {
            SceneManager.nextScene();
        }
     }
}
