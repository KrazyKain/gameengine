﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Interfaces;
using BoardGame.AI;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoardGame.Entities
{
    class playerPiece : Moveable
    {
        private Vector2 Spawn;
        public playerPiece()
        {
            _Brain = new PlayerControl(this);
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            PlayerControl mybrain = (PlayerControl)_Brain;
            Rectangle sourcerec = new Rectangle(0, 0, _Image.Width, _Image.Height);

            //

            spriteBatch.Draw(_Image, _Location, sourcerec, Color.White, mybrain.angle, new Vector2(_Image.Width / 2, _Image.Height / 2), 1f ,SpriteEffects.None,1);
           
        }


        public override Vector2 getVel()
        {
            PlayerControl mybrain = (PlayerControl)_Brain;
            return new Vector2(mybrain.xvel, mybrain.yvel);
        }
        public void setspawn(Vector2 spawn)
        {
            Spawn = spawn;
        }

        public void respawn()
        {
            _Location = Spawn;
        }

        public override void OnCollision(iCollidable other)
        {
            base.OnCollision(other);

            if (other is Monster)
            {
                respawn();
            }
        }
    }
}
