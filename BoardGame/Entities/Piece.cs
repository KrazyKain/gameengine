﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using BoardGame.AI;
using BoardGame.Pathfinding;

namespace BoardGame
{
    class Piece : Moveable, iBluePrint
    {

        public Piece()
        {
            _Brain = new EntityBrain(this);
        }

        public Pathfinder getPathfinder()
        {
            return _Brain._Pathfinder;
        }

        public iBluePrint copy()
        {
            int theuid = EntityManager.generateUID();
            Piece returncopy = new Piece();
            returncopy.initEntity(_uname + theuid, theuid);

            returncopy._Image = _Image;

            return returncopy;
        }
    }
}
