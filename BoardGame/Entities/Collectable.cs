﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Interfaces;
using BoardGame.AI;

namespace BoardGame.Entities
{
    class Collectable : Moveable
    {

        public static int collectablecount;

        public Collectable()
        {
            _brain = new DefaultBehaviour(this);
            collectablecount++;
        }

        public override void OnCollision(iCollidable other)
        {
            base.OnCollision(other);

            if (other is playerPiece)
            {
                _Location = new Microsoft.Xna.Framework.Vector2(9999, 9999);
                collectablecount--;
            }
        }

    }
}
