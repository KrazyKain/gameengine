﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Interfaces;
using Microsoft.Xna.Framework;

namespace BoardGame.Entities
{
    class wall : BGEntity
    {

        public override void OnCollision(iCollidable other)
        {
          
            if (other is Moveable)
            {
                Moveable movobject = (Moveable)other;

                other._Location = Vector2.Subtract(movobject._Location, movobject.getVel());
            }

        }

      
    }
}
