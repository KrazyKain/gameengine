﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

using BoardGame.Interfaces;

namespace BoardGame 
{
    abstract class Entity : IEntity
    {
        public int _UID { get { return _uid; } }//this is the public accessor for _uid, it 
                                                //does not actually containg any data itself, only 
                                                //ensures that _uid is read only externally.
        public String _UName { get { return _uname; } }// same as above for _uname
        public Vector2 _Location { get { return _location; } set { _location = value; } }
        
        protected int _uid; //internally this can be set directly, but externally can only be accessed
                          //via the accessor, which only has a get method, making it read only.
        protected String _uname; //same as above
        protected Vector2 _location;

        public Entity()
        {
            
        }
        
        public void initEntity(String UniqueName, int UID)
        {
            _uname = UniqueName;
            _uid = UID;
        }

        public void update()
        {

        }

        public String toString()
        {
            return _uname;
        }

    }
}
