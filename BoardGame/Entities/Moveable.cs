﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using BoardGame.Interfaces;
using BoardGame.AI;
using BoardGame.Pathfinding;

namespace BoardGame
{
    abstract class Moveable : BGEntity, iMoveable
    {
        public Vector2 _Destination { get { return _destination; } set { _destination = value;} }
        protected Vector2 _destination;

        public float _Speed { get { return _speed; } set { _speed = value; } }
        protected float _speed;

        protected EntityBrain _brain;
        public EntityBrain _Brain { get { return _brain; } set { _brain = value; } }

        private List<iWaypoint> waypoints;
        public List<iWaypoint> _waypoints { get { return waypoints; } set { waypoints = value; } }

        public Moveable()
        {

            _Brain = new EntityBrain(this);
            waypoints = new List<iWaypoint>();

        }

        public virtual Vector2 getVel()
        {
            return Vector2.Zero;
        }

        public void addWaypointToPath(iWaypoint point)
        {
            waypoints.Add(point);
        }

        public List<iWaypoint> getWaypoints()
        {
            return waypoints;
        }

        public void isPlayer()
        {
           
        }

        public void move() //move towards destination
        {

        }
    }
}
