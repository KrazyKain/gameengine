﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BoardGame.Entities
{
    class ViewCone : BGEntity
    {

        public override void draw(SpriteBatch spriteBatch)
        {
          

            spriteBatch.Draw(_Image, _Location, Color.White * 5f);
        }
    }
}
