﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using BoardGame.Interfaces;


namespace BoardGame
{
    abstract class BGEntity : Entity, iRenderable, iCollidable
    {
        public Texture2D _Image { get { return _image; } set { _image = value;} }
        protected Texture2D _image;
       

        public BGEntity()
        { 
        }


        public virtual void draw(SpriteBatch spriteBatch)
        {
            // spriteBatch.Draw(_Image, _Location);
            Rectangle sourcerec = new Rectangle(0, 0, _Image.Width, _Image.Height);
            spriteBatch.Draw(_Image, _Location, sourcerec, Color.White, 0.0f, new Vector2(_Image.Width / 2, _Image.Height / 2), 1f, SpriteEffects.None, 1);
        }

        public virtual void OnCollision(iCollidable other)
        {

        }
    }
}
