﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.AI;
using BoardGame.Interfaces;

namespace BoardGame.Entities
{
    class Door : Moveable
    {
        private string _Exit;

        public Door()
        {
            _brain = new DefaultBehaviour(this);
        }
        public void setExit(string exit)
        {
            _Exit = exit;
        }

        public override void OnCollision(iCollidable other)
        {
            if (other is playerPiece && _Exit!= null )
            {
                playerPiece player = (playerPiece)other;

                player.respawn(); 
                SceneManager.loadScene(SceneManager.getScene(_Exit));

            }
        }
    }
}
