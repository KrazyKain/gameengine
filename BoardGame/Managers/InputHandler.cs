﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Collections;
using BoardGame.Input;

namespace BoardGame.Managers
{

    class InputHandler
    {
        KeyboardState currentkeyState, prevkeyState;

        private static InputHandler _instance = null;
        public static InputHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new InputHandler();
                }
                return _instance;
            }
        }


        public static ArrayList ControllerArray = new ArrayList();
        //needs an arraylist that contains all the ControllerInputs


        public static ControlInput checkLabel(string label)
        {
            ControlInput temp = null;
            foreach (ControlInput keyInput in ControllerArray)
                if (keyInput.checkLabel(label))
                {
                    temp = keyInput;
                }
            return temp;
        }



        public static ControlInput checkKey(Keys key)
        {
            ControlInput newtemp = null;
            foreach (ControlInput keyInput in ControllerArray)
                if (keyInput.checkInput(key))
                {
                    newtemp = keyInput;
                }
            return newtemp;
        }


        public static void addInput(Keys key, String label)
        {

            ControlInput input = new ControlInput(key, label);

            ControllerArray.Add(input);
        }



        public bool keyPressed(params ControlInput[] keys)
        {
            Boolean toReturn = false;
            foreach (ControlInput key in keys)
            {

                if (currentkeyState.IsKeyDown(key._Keyinput) && prevkeyState.IsKeyUp(key._Keyinput))
                {
                    System.Diagnostics.Debug.WriteLine("Pressed");
                    toReturn = true;
                }
            }
            return toReturn;
        }

        public bool checkLabelPressed(String plabel)
        {
            bool toReturn = false;

            ControlInput matchinglabel = checkLabel(plabel);

            toReturn = keyPressed(matchinglabel);


            return toReturn;
        }

        public bool checkLabelDown(String plabel)
        {
            bool toReturn = false;

            ControlInput matchinglabel = checkLabel(plabel);

            Keys thekey = matchinglabel._keyinput;


            toReturn = keyDown(thekey);


            return toReturn;
        }

        public bool checkLabelup(String plabel)
        {
            bool toReturn = false;

            ControlInput matchinglabel = checkLabel(plabel);

            Keys thekey = matchinglabel._keyinput;


            toReturn = keyUp(thekey);


            return toReturn;
        }

        public bool keyUp(params Keys[] keys)
        {
            Boolean toReturn = false;
            foreach (Keys key in keys)
            {
                if (currentkeyState.IsKeyUp(key) && prevkeyState.IsKeyDown(key))
                {
                   
                    return true;
                }
            }
            return toReturn;
        }

        public bool keyDown(params Keys[] keys)
        {
            Boolean toReturn = false;
            foreach (Keys key in keys)
            {
                if (currentkeyState.IsKeyDown(key))
                {
                  
                    return true;
                }
            }
            return toReturn;
        }



        public void Update()
        {
            prevkeyState = currentkeyState;
            //if (!SceneManager.Instance.IsTransitioning)
            currentkeyState = Keyboard.GetState();
        }


    }
}
