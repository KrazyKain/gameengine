﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.Managers;
using Microsoft.Xna.Framework;
using BoardGame.Scenes;
using BoardGame.Interfaces;



namespace BoardGame
{


    static class SceneManager
    {
        static private Scene _currentScene;
        static public Scene _CurrentScene { get { return _currentScene; } set { _currentScene = value; } }
        static private Renderer _renderer;
        static public Renderer _Renderer { set { _renderer = value; } }
        static public List<Scene> _Scenes { get { return _scenes; } set { _scenes = value; } }
        static private List<Scene> _scenes = new List<Scene>();

        static public void addScene(Scene s)
        {
            _scenes.Add(s);
        }

        static public void loadScene(Scene scene)
        {
            QuadTreeManager.Manager().enabled = false;
            _CurrentScene = scene;
            
            _renderer.clearAll();

            foreach(iRenderable renderable in _CurrentScene._BackGroundEntities)
            {
                _renderer.addToRenderList(renderable, RenderLayer.BackGround);
            }
            foreach (iRenderable renderable in _CurrentScene._CanvasEntities)
            {
                _renderer.addToRenderList(renderable, RenderLayer.Canvas);
            }
            foreach (iRenderable renderable in _CurrentScene._ForegroundEntities)
            {
                _renderer.addToRenderList(renderable, RenderLayer.ForeGround);
            }

            AIManager.findbrains(_currentScene);
            QuadTreeManager.Manager().createNode();
            QuadTreeManager.Manager().enabled = true;

        }

        static public void addWaypointToScene(String pscene, iWaypoint waypoint)
        {
            Scene scene = getScene(pscene);
            scene.addWaypoint(waypoint);
        }

        static public void nextScene()
        {
            if (_currentScene != null)
            {
                int currentIndex = _scenes.IndexOf(_currentScene);
                try
                {
                    loadScene(_scenes[++currentIndex]);
                }
                catch {
                } //avoid crashing if no more scenes present
            }
            else
            {
                loadScene(_scenes[0]);
            }

        }

        static public Scene getScene(String name)
        {
            Scene s = null;

            foreach (Scene ss in _scenes)
            {
                if (ss._SceneName.Equals(name))
                {
                    s = ss;
                }
            }

                return s;
        }

        static public Scene getScene(int index)
        {
            return _scenes[index];
        }

        static public void addToScene(Scene S, iRenderable entity, RenderLayer Layer, Vector2 location)
        {
            S.addEntity(entity, Layer, location );

            // also add it to the renderlist considering this is only done on load scene by default

            if (_CurrentScene == S)
            {
                _renderer.addToRenderList(entity, Layer);
            }
            
        }

        static public void addToScene(String S, iRenderable entity, RenderLayer Layer, Vector2 location)
        {
            addToScene(getScene(S), entity, Layer, location);
        }

        static public void removeFromScene(Scene S, iRenderable entity)
        {
            S.removeEntity(entity);
            
        }

        static public void removeFromScene(String S, iRenderable entity)
        {
            removeFromScene(getScene(S), entity);
        }

        public static void createScene(String Name)
        {

            EntityFactory factory = EntityFactory.getFactory();
            Scene S = factory.Build<Scene>();

            S.initScene(Name);

            addScene(S);
        }

    }
}
