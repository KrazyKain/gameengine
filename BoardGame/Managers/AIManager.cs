﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.AI;
using BoardGame.Interfaces;
using BoardGame.Scenes;

namespace BoardGame.Managers
{
    static class AIManager
    {
        static private List<EntityBrain> _activeBrains;
        
        static public void findbrains(Scene activeScene)
        {
            _activeBrains = new List<EntityBrain>();

            findbrains(activeScene._BackGroundEntities);

            findbrains(activeScene._CanvasEntities);

            findbrains(activeScene._ForegroundEntities);


        }

        static public void findbrains(List<iRenderable> Layer)
        {
            foreach (iRenderable r in Layer)
            {
                if (r is iMoveable)
                {
                    iMoveable m = (iMoveable)r;

                    _activeBrains.Add(m._Brain);
                }
            }
        }

        static public void updateBrains()
        {
            foreach (EntityBrain b in _activeBrains)
            {
                b.update();
            }
        }

    }
}
