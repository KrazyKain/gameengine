﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using BoardGame.Interfaces;

namespace BoardGame
{
    static class EntityManager
    {
        //blueprints will contain a copy of all blueprint entities so they can be copied into the entities array
        static private List<iBluePrint> _Blueprints = new List<iBluePrint>(); 
        static private List<IEntity> _Entities = new List<IEntity>();
        static private int _lastUID = 0;
        static private Entity _PlayerToken;
        
        public static void setPlayerToken(Entity Player)
        {
            _PlayerToken = Player;
        }

        public static Entity getPlayerToken()
        {
            return _PlayerToken;
        }

        public static void addBluePrint(iBluePrint bp)
        {
            _Blueprints.Add(bp);
        }

        public static void addEntity(IEntity entity)
        {
            _Entities.Add(entity);
        }

        public static void removeEntity(IEntity entity)
        {
            _Entities.Remove(entity);
        }


        public static void highestUID()
        {
            int highest = 0;
            foreach (IEntity entity in _Entities)
            {
                if (highest > _lastUID)
                {
                    _lastUID = highest;
                }
            }
        }

        public static int generateUID()
        {
            highestUID();
            _lastUID ++;
            return _lastUID;
        }

        public static IEntity createEntity<T>(String Name) where T : class
        {
            EntityFactory Factory = EntityFactory.getFactory();

            IEntity entity = null;
            try
            {
                entity = (IEntity)Factory.Build<T>();

                int UID = generateUID();
                String UName = Name + UID;

                entity.initEntity(UName, UID);

                _Entities.Add(entity);

            }
            catch { }
            return entity;
        }

        public static IEntity createEntity<T>(String Name, Texture2D image) where T : class
        {
            IEntity entity = null;
            try
            {
                entity = createEntity<T>(Name);

                if (entity is iRenderable)
                {
                    iRenderable r = (iRenderable)entity;

                    r._Image = image;

                    entity = (IEntity) r;
                }
            }
            catch { }

            return entity;

        }
 

    }
}
