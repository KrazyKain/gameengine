﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using BoardGame.Collision;
using BoardGame.Scenes;
using BoardGame.Interfaces;



namespace BoardGame.Managers
{
    static class CollisionManager
    {
        
        public static List<Collision.Collision> _Collisions = new List<Collision.Collision>();

        public static void update(Scene currentScene)
        {
        

           
        }

        public static void collisionCheck(List<iCollidable> CollisionObjects)
        {
            _Collisions.AddRange(findCollisions(CollisionObjects));
        }

        public static List<Collision.Collision> findCollisions (List<iCollidable> Layer)
        {
           List<Collision.Collision> Collisions = new List<Collision.Collision>();

           if (Layer.Count() > 1) //don't bother checking for collisions unless there's 2 more more elements.
           {
               foreach (iCollidable entityOne in Layer)
               {
                   foreach (iCollidable entityTwo in Layer)
                   {
                       if (entityOne != entityTwo) //don't bother checking a collision against yourself silly.
                       {
                           if (circleCol(entityOne, entityTwo))  //first, check if they are close to each other
                           {
                               if (squareCol(entityOne, entityTwo))   //then check their bounding boxes, do they overlap?
                               {
                                   if (pixelCol(entityOne, entityTwo))  //now the fun stuff... do the pixels overlap?
                                   {
                                      // System.Diagnostics.Debug.WriteLine("Collision Detected");
                                       Collision.Collision Col = new Collision.Collision(entityOne, entityTwo);

                                       Collisions.Add(Col);
                                        entityOne.OnCollision(entityTwo);
                                   }
                               }
                           }
                       }
                   }
               }
           }


           return Collisions;

        }

        static public Boolean circleCol(iCollidable EntityOne, iCollidable EntityTwo)
        {
            Boolean col = false;

            Vector2 CentreOne = getCentre(EntityOne);
            Vector2 CentreTwo = getCentre(EntityTwo);

            float RadiusOne = getRadius(EntityOne);
            float RadiusTwo = getRadius(EntityTwo);

            float distance =(float) Math.Sqrt(((CentreOne.X - CentreTwo.X) * (CentreOne.X - CentreTwo.X)) +( (CentreOne.Y - CentreTwo.Y) * (CentreOne.Y - CentreTwo.Y))) ;
           

            col = (distance < (RadiusOne + RadiusTwo));

            return col;


        }

        public static float getRadius(iCollidable theEntity)
        {
            Vector2 Centre = getCentre(theEntity);

            float toReturn = 0;
            float x = theEntity._Location.X;
            float y = theEntity._Location.Y;
            float cx = Centre.X;
            float cy = Centre.Y;

            float width = theEntity._Image.Width;
            float height = theEntity._Image.Height;

            toReturn = ((float)Math.Sqrt((((cx) - (x + width)) * ((cx) - (x + width))) + (((cy) - (y)))* ((cy) - (y))));

           

            return toReturn;
        }

        public static Rectangle getBoundingBox(iCollidable entity)
        {
            //if origin in corner
            // Rectangle bounding = new Rectangle((int)entity._Location.X,(int)entity._Location.Y,entity._Image.Height,entity._Image.Width);

            //if origin in center
            Rectangle bounding = new Rectangle((int)entity._Location.X - (entity._Image.Width/2), (int)entity._Location.Y - (entity._Image.Height/2), entity._Image.Width, entity._Image.Height);

          
            return bounding;
        }

        public static Boolean squareCol(iCollidable EntityOne, iCollidable EntityTwo)
        {

            Rectangle boundingOne = getBoundingBox(EntityOne);
            Rectangle boundingTwo = getBoundingBox(EntityTwo);



            return boundingOne.Intersects(boundingTwo);
        }


     

        public static Vector2 getCentre(iCollidable theEntity)
        {
            Vector2 toReturn = theEntity._Location;

            float width = theEntity._Image.Width;
            float height = theEntity._Image.Height;

            float x = toReturn.X + (width/2);
            float y = toReturn.Y + (height / 2);

            toReturn = new Vector2(x, y);

          

            return toReturn;
        }

        public static bool pixelCol(iCollidable entityOne, iCollidable entityTwo)
        {
            

            // Get Color data of each Texture
            Color[] bitsA = new Color[entityOne._Image.Width * entityOne._Image.Height];
            entityOne._Image.GetData(bitsA);
            Color[] bitsB = new Color[entityTwo._Image.Width * entityTwo._Image.Height];
            entityTwo._Image.GetData(bitsB);
            
                        // Calculate the intersecting rectangle
                        int x1 = Math.Max((int)entityOne._Location.X, (int)entityTwo._Location.X);
                        int x2 = Math.Min((int)entityOne._Location.X + (int)entityOne._Image.Width, (int)entityTwo._Location.X + (int)entityTwo._Image.Width);

                        int y1 = Math.Max((int)entityOne._Location.Y, (int)entityTwo._Location.Y);
                        int y2 = Math.Min((int)entityOne._Location.Y + (int)entityOne._Image.Height, (int)entityTwo._Location.Y + (int)entityTwo._Image.Height);
                        


            // For each single pixel in the intersecting rectangle
            for (int y = y1; y < y2; ++y)
            {
                for (int x = x1; x < x2; ++x)
                {
                    // Get the color from each texture
                    Color a = bitsA[(x - (int)entityOne._Location.X) + (y - (int)entityOne._Location.Y) * entityOne._Image.Width];
                    Color b = bitsB[(x - (int)entityTwo._Location.X) + (y - (int)entityTwo._Location.Y) * entityTwo._Image.Width];

                    if (a.A != 0 && b.A != 0) // If both colors are not transparent (the alpha channel is not 0), then there is a collision
                    {
                        return true;
                    }
                }
            }
            // If no collision occurred by now, we're clear.
            return false;
        }

    }
}
