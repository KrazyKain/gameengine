﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoardGame.QuadTree;
using BoardGame.Scenes;
using BoardGame.Interfaces;



namespace BoardGame.Managers
{
    class QuadTreeManager
    {
        private static QuadTreeManager _Manager = null;
        private Node _NodeZero;
        private Renderer _Renderer;
        public bool enabled;
        
        private QuadTreeManager()
        {
            System.Diagnostics.Debug.WriteLine("Quadtree Manager created");
            _Renderer = Renderer._Renderer();
        }

        //singleton!
        public static QuadTreeManager Manager()
        {
            if (_Manager == null)
            {
                _Manager = new QuadTreeManager();
            }

                return _Manager;
        }

        public void createNode()
        {
            //enabled = true;
            //creates the first node taking all currently active Entities on the canvas as collidables, the resolution as the initial size and starting at the origin point to cover the whole screen
            _NodeZero = new Node(convertRenderToCollide(SceneManager._CurrentScene._CanvasEntities), _Renderer.getResolution(), new Microsoft.Xna.Framework.Vector2(0, 0));
            System.Diagnostics.Debug.WriteLine("first node created");

        }

        private List<iCollidable> convertRenderToCollide(List<iRenderable> prenders)
        {
            List<iCollidable> collidables = new List<iCollidable>();

            foreach (iRenderable r in prenders)
            {
                iCollidable c = (iCollidable)r;

                if (c!= null)
                {
                    collidables.Add(c);
                }
            }

            return collidables;
        }

        public void checkNode()//parameters: node and object.
        {
            //check if an object is in a node.
        }

        public void update()
        {
            if (enabled)
            {
                _NodeZero.update();
            }
        }
    }
}
