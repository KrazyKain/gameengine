﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BoardGame.Input
{
    class ControlInput
    {

        public Keys _keyinput;
        private String _label;

        public Keys _Keyinput { get { return _keyinput; } set { _keyinput = value; } }


        public String _Label { get { return _label; } }

        public ControlInput(Keys theInput, String theLabel)
        {
            _keyinput = theInput;
            _label = theLabel;
        }

        public bool checkInput(Keys keyInput)
        {
            if (keyInput == _keyinput)
            { 
                return true;
            }
            else
            {
               
                return false;
            }
        }

        public bool checkLabel(String theLabel)
        {
            if (theLabel == _label)
            {
               
                return true;
            }
            else
            {
               
                return false;
            }
        }


    }
}
