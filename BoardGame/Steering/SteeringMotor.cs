﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using BoardGame.Pathfinding;
using BoardGame.Interfaces;
using BoardGame.AI;

namespace BoardGame.Steering
{
    class SteeringMotor : iSteering
    {
        private Vector2 steeringVelocity;
        private float maxForce = 0.25f;
        private float maxSpeed = 3;
        
        public Vector2 seekTarget(Vector2 position, Vector2 target)
        {
            Vector2 pathToTarget = new Vector2();

            //Get initial direction between where the entity is and where it wants to go
            pathToTarget = Vector2.Subtract(target, position);

            //Get single unit vector of direction the entity wants to travel
            pathToTarget = Vector2.Normalize(pathToTarget);
            
            //multiply by maxspeed to limit how fast it travels
            pathToTarget = Vector2.Multiply(pathToTarget, maxSpeed);

            //Used to start turning the entity towards the target
            Vector2 force = Vector2.Subtract(pathToTarget, steeringVelocity);
           
            //Used to limit the magnitude of the steeringForce, stops the turning radius from being too wide
            if (Vector2.Normalize(force).Length() > maxForce)
            {
                force = Vector2.Normalize(force);
                force = Vector2.Multiply(force, maxForce);
            }
             
            //add the velocity from last step to the new steering velocity
            steeringVelocity = Vector2.Add(steeringVelocity, force);
            return steeringVelocity;
        }


        public Vector2 getVelocity()
        {
            return steeringVelocity;
        }

        //Calculates an estimate for where the enemy is likely to be travelling based on its current position and velocity
        public Vector2 pursueTarget(Vector2 targetPosition, Vector2 targetVelocity)
        {
            //Scalar used to change the magnitude of the vector. 
            float scale = 0.2f;
            //The position of where the entity needs to travel is calculated below - the scale controls how far ahead it will travel
            Vector2 estimatedPostion = Vector2.Add(targetPosition, targetVelocity * scale);
            //The returned position can be fed into seek in order to smooth the path
            return estimatedPostion;
        }




    }
}
